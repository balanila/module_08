#!/bin/sh
#ansible frontend_servers -m apt -a 'name="python3"' -b
#ansible frontend_servers -m apt -a 'name="python3-pip"' -b
#ansible frontend_servers -m shell -a "pip3 install boto3"
ANSIBLE_LOAD_CALLBACK_PLUGINS=true ANSIBLE_STDOUT_CALLBACK=json ansible localhost \
	-m community.aws.ec2_instance_info -a \
	'region=us-east-1 filters=tag:Name="Nginx Server in Auto Scalling Group"' | jq -r \
	".plays[0] .tasks[0] .hosts .localhost .instances[] .public_ip_address"
